library(shiny)
library(shinydashboard)
library(DT)
library(plyr)
library(plotly)
library(data.table)
load("route888data.RData")
load("stan_model.RData")
tpx_est <- as.data.table(round(summary(m1)[,c("50%", "2.5%", "97.5%")], 1))
tpx_est[, Var := rownames(summary(m1))]
tpx_est[2:14, Term := substr(Var, regexpr(':', Var) + 1, regexpr("]", Var) - 1)]
tpx_est <- tpx_est[, c('Var', 'Term', '50%', '2.5%', '97.5%')]
setnames(tpx_est, names(tpx_est), c("Var", "Term", "Estimate (ft)", "LCI", "UCI"))

ui <- dashboardPage(skin = "black",
  dashboardHeader(title = tagList(img(src = "mtlogo.png", height = "45px"), "Northstar Odometer"), titleWidth = 300),
  title = "Northstar Odometer",
  dashboardSidebar(width = 300, 
                   uiOutput("data_direction"),
                   uiOutput("data_service"),
                   uiOutput("data_timePoint"),
                   checkboxInput("tp_breakout", "Breakout Time Points"),
                   uiOutput("data_trains")),
  dashboardBody(
    tags$style(HTML(".sidebar { height: 97vh; overflow-y: auto; position: fixed;}
                    .form-group{ margin-bottom: 0px; }
                    .selectize-control{ margin-bottom: 0px; }
                    div.content-wrapper { overflow-y: auto; }
                    .main-header .logo { text-align: left; font-size: 14px; }
                    .small-box.bg-blue {min-height: 125px; background-color: #0053A0 !important; color: #ffffff !important; }
                    .small-box.bg-yellow { background-color: #EBCA23 !important; color: #000000 !important; }
                    .small-box.bg-red { background-color: #ED1B2E !important; color: #ffffff !important; }
                    .info-box.bg-blue {min-height: 125px; background-color: #0053A0 !important} 
                    .info-box.bg-red {min-height: 125px; background-color: #ED1B2E !important; color: #ffffff !important;} 
                    .info-box.bg-yellow {min-height: 125px; background-color: #EBCA23 !important; color: #ffffff !important;} 
                    .info-box-icon {height: 125px; line-height: 125px;} 
                    .info-box-content {padding-top: 0px; padding-bottom: 0px;}
                    "
      )),
    valueBox("Northstar Commuter Rail Line Odometer Analysis", 
             subtitle = 'Scheduled distance vs odometer distance on route 888', 
             width = 12, color = "yellow", icon = icon("train")), 
    fluidRow(
      column(width = 2, 
             valueBox(tags$p("Take Away", style = "font-size: 75%;"), "No evidence of odometer error was found", color = "blue", width = 12), 
             valueBoxOutput("intercept", width = 12)), 
      column(width = 5, box(title = "Segment Discrepancies", 
                            p("This table shows the average discrepancy (Odometer - Sched Dist) in ft associated with a segment, adjusting for potential effects of the odometer. The model suggests that typically trains do not travel as far as the scheduled distance at the BAPK time point. Conversely, trains tend to travel more than the scheduled distance at the ERST time point. The other time points do not show strong evidence of consistent discrepancies."), 
                            br(),
                            dataTableOutput("tpx_table"), width = 12)), 
      column(width = 5, box(title = "Odometer Discrepancies",
                            p("This table shows the average discrepancy (Odometer - Sched Dist) in ft associated with the train's odometer, adjusting for potential effects of the segment. None of the trains show evidence of an inaccurate odometer after adjustments."), 
                            br(),
                            dataTableOutput("segment_table"), width = 12))
      ),
    br(), br(), 
    valueBox("Data Visualization", 
             subtitle = 'Explore the data', 
             width = 12, color = "yellow", icon = icon("train")), 
    infoBoxOutput("train_title", width = 3), 
    infoBoxOutput("time_point", width = 3), 
    valueBoxOutput("average", width = 2), 
    valueBoxOutput("median", width = 2), 
    valueBoxOutput("n", width = 2), 
    box(width = 12, plotlyOutput("plot", height = "650px")), 
    box(width = 4,
        p("Analysis performed for Jason Podany and Gary Nyberg on 15 Oct 2018."), 
        p(paste0("Date range of data is from ", sched_tpx[, min(REVISED_LOCAL_TIMESTAMP)], " to ", sched_tpx[, max(REVISED_LOCAL_TIMESTAMP)], ".")),
        p("Analysis performed by", tags$a(href='mailto:brandon.whited@metrotransit.org', "Brandon Whited."),  "Please contact with with questions or comments."))
    
    ) #end dashboardBody
) #end dashboardPage



server <- function(input, output) {
  
  output$data_direction <- renderUI({
    temp_choices <- sched_tpx[, unique(ROUTE_DIRECTION_ABBR)]
    checkboxGroupInput("direction", "Select Direction", 
                       choices = temp_choices, inline = T, selected = temp_choices)
  })
  
  output$data_service <- renderUI({
    temp_choices <- sched_tpx[, unique(SERVICE_ABBR)]
    checkboxGroupInput("service", "Select Day", 
                       choices = temp_choices, inline = T, selected = temp_choices)
  })

  output$data_timePoint <- renderUI({
    temp_choices <- sched_tpx[, unique(LM_TIMEPOINT_ABBR)]
    checkboxGroupInput("timePoint", "Select Time Points", choices = temp_choices, selected = temp_choices, inline = F)
  })
  
  output$data_trains <- renderUI({
    temp_choices <- sched_tpx[, unique(LM_PROPERTY_TAG)]
    checkboxGroupInput("train", "Select Trains", choices = temp_choices, selected = temp_choices[1], inline = F)
  })

  
  dat_ss <- reactive({
    req(input$direction, input$timePoint, input$service, input$train)
    
    temp_dat <- droplevels(sched_tpx[
      ROUTE_DIRECTION_ABBR %in% input$direction & 
        SERVICE_ABBR %in% input$service & 
        LM_TIMEPOINT_ABBR %in% input$timePoint &
        LM_PROPERTY_TAG %in% input$train & 
        abs(dist_diff) < 600 ])
    
    temp_dat <- temp_dat[dist_tpx > 0 & SCHED_DIST_FROM_LAST_GEO_NODE > 0]
    setkey(temp_dat, REVISED_LOCAL_TIMESTAMP)
    temp_dat[, ID := 1:.N, .(LM_PROPERTY_TAG)]
    temp_dat[, LM_TIMEPOINT_ABBR := factor(LM_TIMEPOINT_ABBR, levels = c('BLST', 'ERST', 'RMST', 'ANST', 'CRNS', 'FRST', 'BAPK'))]
    return(temp_dat)
  })

  output$plot = renderPlotly({
    p <- ggplot(dat_ss(), aes(x = dist_diff, fill = LM_PROPERTY_TAG)) + 
      geom_histogram(bins = 30) + guides(fill = guide_legend(title = "Train")) + 
      xlab("Odometer - Sched Dist (ft)") + 
      geom_vline(xintercept = 0)  + 
      # scale_x_continuous(breaks = pretty(dat_ss()$dist_diff, n = 12)) + 
      theme_minimal()
    
    if(input$tp_breakout){
      p <- p + facet_wrap(LM_TIMEPOINT_ABBR ~ LM_PROPERTY_TAG)
    }else{
      p <- p + facet_wrap(~LM_PROPERTY_TAG)
    }
    
    p <- ggplotly(p)
    p$elementId <- NULL
    p
  })

  output$train_title <- renderInfoBox({
    infoBox("LM PROPERTY TAG ", paste0(input$train, collapse = ", "), icon = icon("train"), color = "blue", fill = T)
  })

  output$time_point <- renderInfoBox({
    infoBox("Time Point ", paste0(input$timePoint, collapse = ", "), icon = icon("stop-circle"), color = "blue", fill = T)
  })

  output$average <- renderValueBox({
    valueBox(dat_ss()[, paste(round(mean(dist_diff)), "ft")], "Mean diff. from schedule for selection", icon = icon("tachometer"), color = "blue")
  })
  
  output$median <- renderValueBox({
    valueBox(dat_ss()[, paste(median(dist_diff), "ft")], "Median diff. from schedule for selection", icon = icon("tachometer"), color = "blue")
  })
  
  output$n <- renderValueBox({
    valueBox(dat_ss()[, .N], "Number of data points", icon = icon("plus"), color = "blue")
  })

  
  
  
  output$intercept <- renderValueBox({
   valueBox(tags$p(paste(tpx_est[Var == "(Intercept)", "Estimate (ft)"], "ft"), style = "font-size: 75%;"), 
            "Estimated discrepancy (Odometer - Sched Dist) regardless of segment or train car",
            icon = icon("tachometer"), color = "blue")
  })
  
  output$tpx_table <- renderDataTable({
    tpx_est[grepl("LM_TIMEPOINT_ABBR", Var) & !grepl("Sigma", Var)][, -1]
  })
  
  output$segment_table <- renderDataTable({
    tpx_est[grepl("LM_PROPERTY_TAG", Var) & !grepl("Sigma", Var)][, -1]
  })
  
  }

shinyApp(ui, server)









